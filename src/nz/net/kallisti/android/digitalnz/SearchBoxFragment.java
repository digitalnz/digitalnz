package nz.net.kallisti.android.digitalnz;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This fragment provides the search box to the user.
 * 
 * @author robin
 */
public class SearchBoxFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_search_box, container, false);
		return rootView;
	}
	
}
